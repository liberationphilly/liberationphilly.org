from django import template
from django.utils.safestring import mark_safe
from django.utils import timezone
from events.models import Event
from website.event_page.models import EventPage
from django.utils.timezone import now
from datetime import timedelta

register = template.Library()


@register.simple_tag
def format_dates(start, end, multiline=False):
    local_start = timezone.localtime(start)
    local_end = timezone.localtime(end)

    # if start and end time is on the same day
    if local_start.date() == local_end.date():
        date_format = "%A %b %d"
        time_format = "%I:%M%p"
        # Tuesday Jan 2, 1:00PM–4:00PM
        template_string = "{date}, {start_time} – {end_time}"
        if multiline:
            template_string = "{date}<br>{start_time} – {end_time}"
        return mark_safe(template_string.format(
            date=local_start.date().strftime(date_format),
            start_time=local_start.time().strftime(time_format).lower(),
            end_time=local_end.time().strftime(time_format).lower()
        ))

    # if start and end time is on different
    # days but less than or equal to 24 hours apart
    elif local_end - local_start <= timedelta(days = 1):
        date_format = "%A %b %d"
        time_format = "%I:%M%p"

        # Tuesday Jan 2, 10:00PM – Wednesday Jan 3, 11:00PM
        template_string = "{start_date}, {start_time} – {end_date}, {end_time}"
        if multiline:
            template_string = "{start_date}, {start_time}<br>{end_date}, {end_time}"
        return mark_safe(template_string.format(
            start_date=local_start.date().strftime(date_format),
            start_time=local_start.time().strftime(time_format).lower(),
            end_date=local_end.date().strftime(date_format),
            end_time=local_end.time().strftime(time_format).lower()
        ))

    # if start and end time is on different days and greater than 24 hours apart
    else:
        date_format = "%A %b %d, %Y"

        # Tuesday Jan 2, 2018 – Saturday Jan 6, 2018
        template_string = "{start_date} – {end_date}"
        if multiline:
            template_string = "{start_date}<br>{end_date}"
        return mark_safe(template_string.format(
            start_date=local_start.date().strftime(date_format),
            end_date=local_end.date().strftime(date_format),
        ))


@register.simple_tag
def filtered_events(tag):
    """
    Returns the next 3 events with the given tag.
    """
    events = EventPage.objects.filter(tags__id=tag.id, end_time__gte=now())[:3]
    return events


@register.simple_tag
def home_events():
    """
    Returns the next 4 events.
    """
    events = EventPage.objects.filter(end_time__gte=now()).order_by('start_time')[:4]
    return events


@register.simple_tag
def upcoming_events():
    """
    Returns the next 4 events.
    """
    events = EventPage.objects.filter(end_time__gte=now()).order_by('start_time')
    return events
