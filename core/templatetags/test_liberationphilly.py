from django.test import TestCase
from datetime import datetime
from pytz import UTC
from .liberationphilly import format_dates
from django.utils import timezone


class InputOutputFormatDates:

    def __init__(self, name, start, end, multiline, expected):
        self.name = name
        self.start = start
        self.end = end
        self.multiline = multiline
        self.expected = expected

    def get_local_start(self):
        local_start = timezone.localtime(self.start)
        return local_start.time().strftime("%I:%M%p").lower()

    def get_local_end(self):
        local_end = timezone.localtime(self.end)
        return local_end.time().strftime("%I:%M%p").lower()

    def get_expected(self):
        return self.expected.format(
            start=self.get_local_start(),
            end=self.get_local_end())


class LiberationPhillyTestCase(TestCase):

    def test_format_dates(self):
        tests = [
            # the nominal cases
            InputOutputFormatDates(
                "Same day, w/o multiline",
                datetime(2018, 7, 20, 10, 20, 0, 0, UTC),
                datetime(2018, 7, 20, 12, 20, 0, 0, UTC),
                False,
                # Expectation:
                "Friday Jul 20, {start} – {end}"
            ),
            InputOutputFormatDates(
                "Same day, w/ multiline",
                datetime(2018, 7, 20, 10, 20, 0, 0, UTC),
                datetime(2018, 7, 20, 12, 20, 0, 0, UTC),
                True,
                # Expectation:
                "Friday Jul 20<br>{start} – {end}"
            ),

            # the "late party" cases :)
            InputOutputFormatDates(
                "Different day and less than 24 hours, w/o multiline",
                # This will be Friday Jul 20 @ 11 PM EDT
                datetime(2018, 7, 21, 3, 0, 0, 0, UTC),
                datetime(2018, 7, 21, 6, 0, 0, 0, UTC),
                False,
                # Expectation:
                "Friday Jul 20, {start} – Saturday Jul 21, {end}"
            ),
            InputOutputFormatDates(
                "Different day and less than 24 hours, w/ multiline",
                # This will be Friday Jul 20 @ 11 PM EDT
                datetime(2018, 7, 21, 3, 0, 0, 0, UTC),
                datetime(2018, 7, 21, 6, 0, 0, 0, UTC),
                True,
                # Expectation:
                "Friday Jul 20, {start}<br>Saturday Jul 21, {end}"
            ),

            # the multiday conference cases
            InputOutputFormatDates(
                "More than 24 hours, w/o multiline",
                datetime(2018, 7, 20, 5, 20, 0, 0, UTC),
                datetime(2018, 7, 21, 5, 21, 0, 0, UTC),
                False,
                # Expectation:
                "Friday Jul 20, 2018 – Saturday Jul 21, 2018"
            ),
            InputOutputFormatDates(
                "More than 24 hours, w/ multiline",
                datetime(2018, 7, 20, 5, 20, 0, 0, UTC),
                datetime(2018, 7, 21, 5, 21, 0, 0, UTC),
                True,
                # Expectation:
                "Friday Jul 20, 2018<br>Saturday Jul 21, 2018"
            ),

        ]

        for test in tests:
            expected = test.get_expected()
            actual = format_dates(test.start, test.end, test.multiline)
            self.assertEqual(
                expected,
                actual,
                "\"{test_name}\" test case failed.".format(
                    test_name = test.name
                )
            )
