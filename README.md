![screenshot from 2018-04-01 13 34 40](https://user-images.githubusercontent.com/17955536/38175854-792746a2-35b1-11e8-8c1a-6ab977e7d129.png)

Liberation Philly
=================

[![Maintainability](https://api.codeclimate.com/v1/badges/222dd743841a45af937f/maintainability)](https://codeclimate.com/github/liberationphilly/liberationphilly)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)


A website for Liberation Philly in Django and Wagtail. Events, users, and email list are managed through Django at /admin. Website content is managed through wagtail at /cms.

We are working on making this site as generic and flexible as possible so other Liberation Collective groups (or anyone) may use the template for their own organization with limited (or no) code changes needed. Please submit an issue for any suggestions you have!

---

## Getting set up locally:

### Prerequisites
1. Python 3.x.x
2. pip3
3. Linux or MacOS (these instructions are not for Windows)

**1. Install required pip packages**
```
pip3 install virtualenvwrapper
```
**2. Update .bashrc**
Append the following to your `.bashrc` file:
```
# All your virtualenvs will get placed here
export WORKON_HOME=$HOME/.virtualenvs
# All your python projects that have an associated virtualenv will get placed here
export PROJECT_HOME=$HOME/python_repos
# Tells virtualenvwrapper which python interpreter to use
export VIRTUALENVWRAPPER_PYTHON="$(which python3)"
# Tells virtualenvwraper which virtualenv to use
export VIRTUALENVWRAPPER_VIRTUALENV="$(which virtualenv)"
source virtualenvwrapper.sh
```
After you are finished run:

```
source ~/.bashrc
```

**3. Create a virtualenvwrapper project. This will create a folder in `PROJECT_HOME` for the source of this project and create a virtualenv for it.**
```
makeproject liberationphilly
```
**4. After `makeproject` you will be in `$PROJECT_HOME/liberationphilly`. Now clone this project.**

```
git clone https://github.com/liberationphilly/liberationphilly.git .
```

**5. Install the requirements**
```
pip install -r requirements.txt
```

**6. Migrate (this creates the database tables you'll need)**
```
python manage.py migrate
```

**7. Run the server.**
```
python manage.py runserver
```
After this you should be able to view a landing page at http://127.0.0.1:8000/ which says "Welcome to your new wagtail site!". To start creating the pages, follow to the next section:

---

## Creating your Liberation Home Page:

**1. Create a Super User account and log into the CMS**
(If you started the server in the previous step, you'll need to quit it with CTRL+C)

```
$ python3 manage.py createsuperuser
```

```
Username: admin
```

```
Email address: admin@example.com
```

```
Password: **********
Password (again): *********
Superuser created successfully.
```
Restart the server `python3 manage.py runserver`.

Go to: http://127.0.0.1:8000/cms/ and login with the credentials you just created.

![screenshot from 2018-04-01 11 52 36](https://user-images.githubusercontent.com/17955536/38175717-0dfa34e0-35af-11e8-8fb5-6e7438dd62b4.png)


**2. Delete the default Wagtail Home Page**

To do this, simply click the "Welcome to your new Wagtail site!" page, and select "delete" from the drop down options. You sould see a page that looks like this when youre done:

![screenshot from 2018-04-01 11 57 13](https://user-images.githubusercontent.com/17955536/38175718-0e08e58a-35af-11e8-9074-a565acf92f40.png)


**3. Create your Home Page and basic info**

Under where it says "Root" click "Add Child Page" -> "Home Page".

From here you will fill in all of the basic info for your site fields including:

 - Title: The name of your site. (ex. Liberation Philadelphia)
 - Title Left and Title Right: These are the words that will show on either side of your logo in the header banner.
 - Logo: This is the logo that will show in your header banner. (you'll probably want to use a white or otherwise light logo)

![screenshot from 2018-04-01 12 02 32](https://user-images.githubusercontent.com/17955536/38175719-0e16ca38-35af-11e8-9743-97552a8e94f8.png)

 Note: currently the banner image is hard coded and can not be changed in the CMS. Please update the static files at /website/core/static/images to change the banner image.

Go to the "Promote" tab and click "Show in Menus".


**4. Adding content to your home page**

The home page automatically generates a calendar which displays up to 4 upcoming events (if there are any). Before you can publish the page, a body is required. The home page is structured into "Info Sections" which include a Header, Paragraphs, and a Call To Action Button.

![screenshot from 2018-04-01 12 10 36](https://user-images.githubusercontent.com/17955536/38175720-0e258564-35af-11e8-97b2-99513b02445c.png)

Go back to the "Content" tab, and in the "body" section click "Info Section".

From here you will be able to write your home page content. To add new Info Sections, click the small "+" below the current section.

When you are satisfied with your content, select "publish" from the green drop down in the footer. This should bring you to a page that looks like this:

![screenshot from 2018-04-01 12 14 22](https://user-images.githubusercontent.com/17955536/38175723-0e59ef20-35af-11e8-85e5-b24dd2e14022.png)


**5. Configuring your site root**

From here, click "configure a site now" and fill out this form to configure your site. Select the home page you just made as the site Root, and select "Is default site". Click Save.

![screenshot from 2018-04-01 12 17 08](https://user-images.githubusercontent.com/17955536/38175724-0e7cc180-35af-11e8-87c9-24d796895e50.png)

Navigate back to your pages on the gray left sidebar, and you should be able to view the live homepage now!

---

## Creating Child Pages:

**1. Adding your first child page**

From the root directory, choose "add a child page" beneath your home page:

![screenshot from 2018-04-01 12 20 31](https://user-images.githubusercontent.com/17955536/38175725-0e97d39e-35af-11e8-8cbc-856212669dda.png)


**2. Chosing your page type**

You will be presented with a choice of what type of page you would like to make.

 - Standard Page: *The most common page type, this page is used for sharing information about a particular topic (for example, Anonymous for the Voiceless or Save). It allows you to create Info Sections, similar to the home page, as well as add images either through a large Featured Image block, or smaller stylized Polaroid blocks. The Standard Page also includes an optional sidebar which allows you to pull in events filtered by a specific tag (more on this later).*
 - Events Index Page: *This page automatically generates a list of all upcoming events and displays them in a filterable list.*
 - Contact Page: *This page allows you to add some pictures and content, but additionally allows you to add a contact form.*

Note on Contact page: currently the template is hard coded to send the email to: mk@marykatefain.com. You will need to manually edit this in the template at: website/core/templates/core/blocks/contact_form.html.

You will likely want to start by creating multiple Standard Pages around particular topics, and then adding and Events and Contact page.


**3. Managing your Page Tree**

Pages will show in the top nav in the order they are created. Only pages which have "show in menus" ticked in the "Promote" tab will be shown in the nav. This is unchecked by default.

The site template currently does not support subpages in the top nav. We hope to add this feature soon!


**4. Understanding Block Types:**

There are many block types, each which include thier own content fields, styles, and templates.

Each of these tempaltes can be changed at: website/core/templates/core/blocks

 - Info Section: *Just like on the Home Page, the Info Section block contains a header, paragraphs, and optionally a call to action button.*
![screenshot from 2018-04-01 12 55 36](https://user-images.githubusercontent.com/17955536/38175726-0ea59696-35af-11e8-936c-5801e017240e.png)

 - Featured Image: *Diplays a large image horizontally centered on the page*
![screenshot from 2018-04-01 13 00 04](https://user-images.githubusercontent.com/17955536/38175728-0ec04c0c-35af-11e8-854d-d196fcb24e6d.png)

 - Polaroids: *Creates a stylized row of mutiple smaller pictures*
![screenshot from 2018-04-01 12 57 52](https://user-images.githubusercontent.com/17955536/38175727-0eb204da-35af-11e8-8638-e42e87938bf8.png)

 - Contact Form: *(Only available on Contact Page, see above)*
![screenshot from 2018-04-01 13 33 39](https://user-images.githubusercontent.com/17955536/38175846-52399982-35b1-11e8-91c9-d8adb1c1049f.png)

- Events Filter (Only available in sidebar): *Generates a list of upcoming events based on a speciic tag (ex. "Cube").*
![screenshot from 2018-04-01 13 00 46](https://user-images.githubusercontent.com/17955536/38175729-0ecdcaf8-35af-11e8-94f6-2aa44eeb1b4c.png)

---

## Deloying to liberationphilly.org:

```
git push dokku master
```
Note: ssh access required. Check if dokku has been added as a remote with `git remote -v`


---

## License:
This website is licensed under the the AGPL 3.0. View the [license](LICENSE) file for full details.
