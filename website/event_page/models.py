from django.db import models

from wagtail.core.models import Page

from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase

from ckeditor.fields import RichTextField

from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel

from wagtail.images.edit_handlers import ImageChooserPanel


from wagtailmodelchooser import register_model_chooser


class EventPageTag(TaggedItemBase):
    content_object = ParentalKey(
        'EventPage',
        related_name='tagged_items',
        on_delete=models.CASCADE
    )


class EventPage(Page):
    """
    A Liberation Philly event.
    """
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    address = RichTextField()
    description = RichTextField()
    link = models.URLField(max_length=255, blank=True)
    tags = ClusterTaggableManager(through=EventPageTag, blank=True)
    contact = models.EmailField(max_length=255)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    ICON_CHOICES = (
        ('calendar', 'Calendar'),
        ('save', 'Save'),
        ('cube', 'Cube'),
        ('protest', 'Protest'),
        ('training', 'Training'),
        ('social', 'Social'),
    )
    icon = models.CharField(
        max_length=255,
        choices=ICON_CHOICES,
        default='calendar',
    )


    def __str__(self):
        return self.title

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('start_time'),
                FieldPanel('end_time'),
                FieldPanel('address'),
                FieldPanel('description'),
                FieldPanel('link'),
                FieldPanel('tags'),
                FieldPanel('contact'),
                ImageChooserPanel('image'),
                FieldPanel('icon'),
            ]
        ),
    ]

    parent_page_types = ['events_index_page.EventsIndexPage']
