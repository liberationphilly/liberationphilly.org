# Generated by Django 2.0.1 on 2018-06-21 18:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('event_page', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='eventpage',
            old_name='address_title',
            new_name='address',
        ),
        migrations.RemoveField(
            model_name='eventpage',
            name='level',
        ),
        migrations.RemoveField(
            model_name='eventpage',
            name='name',
        ),
        migrations.AlterField(
            model_name='eventpage',
            name='image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.Image'),
        ),
        migrations.AlterField(
            model_name='eventpage',
            name='tags',
            field=models.ManyToManyField(blank=True, to='events.Tag'),
        ),
        migrations.DeleteModel(
            name='Level',
        ),
    ]
