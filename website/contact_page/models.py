from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.images.blocks import ImageChooserBlock
from website.core.blocks import InfoSection, PolaroidBlock, ContactForm, SubscribeBlock

from wagtailmodelchooser.blocks import ModelChooserBlock


class ContactPage(Page):
    """
    A page where users can submit a contact form or find contact info.
    """

    # Overrides the title displayed on the inner page
    # `title` is used for nav, `page_title` for inner pages
    page_title = models.CharField(max_length=30)

    body = StreamField(
        [
            ('infosection', InfoSection()),
            ('featuredimage', ImageChooserBlock()),
            ('Polaroids', PolaroidBlock()),
            ('ContactForm', ContactForm()),
            ('Subscribe', SubscribeBlock()),
        ]
    )

    content_panels = Page.content_panels + [
        FieldPanel('page_title'),
        StreamFieldPanel('body'),
    ]
