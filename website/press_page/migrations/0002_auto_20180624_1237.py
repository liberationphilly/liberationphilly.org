# Generated by Django 2.0.1 on 2018-06-24 16:37

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('press_page', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='presspage',
            name='body',
            field=wagtail.core.fields.StreamField((('Press', wagtail.core.blocks.StructBlock((('headline', wagtail.core.blocks.CharBlock(required=False)), ('snippet', wagtail.core.blocks.CharBlock(max_length=250, required=False)), ('url', wagtail.core.blocks.URLBlock(required=False)), ('image', wagtail.images.blocks.ImageChooserBlock())))),)),
        ),
    ]
