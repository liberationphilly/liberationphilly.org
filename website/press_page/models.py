from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel

from wagtailmodelchooser.blocks import ModelChooserBlock

from website.core.blocks import DonateBlock, SidebarCTABlock, SubscribeBlock, PressBlock


class PressPage(Page):
    """
    A page which lists recent press liberation philly has recieved
    """

    # Overrides the title displayed on the inner page
    # `title` is used for nav, `page_title` for inner pages
    page_title = models.CharField(max_length=30)

    body = StreamField(
        [
            ('Press', PressBlock()),
        ]
    )

    sidebar = StreamField(
        [
            ('event_filter', ModelChooserBlock(
                'events.Tag',
                template="core/blocks/sidebar_event.html",
            )),
            ('Donate', DonateBlock()),
            ('CTA', SidebarCTABlock()),

        ],
        blank=True
    )

    content_panels = Page.content_panels + [
        FieldPanel('page_title'),
        StreamFieldPanel('body'),
        StreamFieldPanel('sidebar'),
    ]
