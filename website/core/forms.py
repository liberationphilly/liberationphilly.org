from django import forms


class EmailForm(forms.Form):
    email = forms.CharField(
        label='Email',
        widget=forms.TextInput(attrs={'placeholder': 'yourname@email.com'}),
        max_length=255
    )
