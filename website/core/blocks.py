from django.db import models

from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock

from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase


class InfoSection(blocks.StructBlock):
    headline = blocks.CharBlock(required=False)
    body = blocks.RichTextBlock(required=False)
    call_to_action_target = blocks.PageChooserBlock(required=False)
    call_to_action_text = blocks.CharBlock(required=False)

    class Meta:
        template = 'core/blocks/info_section.html'
        icon = 'fa-paragraph'


class PolaroidBlock(blocks.ListBlock):
    child_block = ImageChooserBlock()

    def __init__(self, **kwargs):
        super().__init__(child_block=self.child_block, **kwargs)

    class Meta:
        template = 'core/blocks/polaroid_block.html'
        icon = 'fa-camera-retro'


class ContactForm(blocks.StructBlock):

    class Meta:
        template = 'core/blocks/contact_form.html'
        icon = 'fa-phone'


class SubscribeBlock(blocks.StructBlock):

    class Meta:
        template = 'core/blocks/subscribe_form.html'
        icon = 'fa-envelope'


class DonateBlock(blocks.StructBlock):

    class Meta:
        template = 'core/blocks/donate.html'
        icon = 'fa-money'


class SidebarCTABlock(blocks.StructBlock):
    headline = blocks.CharBlock(required=False)
    body = blocks.RichTextBlock(required=False)
    call_to_action_target = blocks.URLBlock(required=False)
    call_to_action_text = blocks.CharBlock(required=False)
    image = ImageChooserBlock()


    class Meta:
        template = 'core/blocks/sidebar_cta.html'
        icon = 'fa-bullhorn'


# class PressTag(TaggedItemBase):
#     content_object = ParentalKey(
#         'PressBlock',
#         related_name='tagged_items',
#         on_delete=models.CASCADE
#     )
#

class PressBlock(blocks.StructBlock):
    headline = blocks.CharBlock(required=False)
    published = blocks.DateBlock()
    snippet = blocks.CharBlock(required=False, max_length=250)
    url = blocks.URLBlock(required=False)
    image = ImageChooserBlock()
    # tags = ClusterTaggableManager(through=PressTag, blank=True)


    class Meta:
        template = 'core/blocks/press_item.html'
        icon = 'fa-bullhorn'
