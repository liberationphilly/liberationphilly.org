from django.db import models

from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel

from website.core.forms import EmailForm
from django.http import HttpResponseRedirect

from events.models import Email


class EventsIndexPage(Page):
    """
    A page which lists all upcoming events and their details.
    """

    def get_context(self, request):
        context = super().get_context(request)
        context['form'] = EmailForm()
        return context

    def serve(self, request):
        if request.method == "POST":
            # create a form instance and populate it with data from the request:
            form = EmailForm(request.POST)
            # check whether it's valid:
            if form.is_valid():
                # process the data in form.cleaned_data as required
                email = form.cleaned_data['email']
                Email.objects.create(
                    email=email,
                )
        return super().serve(request)

    # Overrides the title displayed on the inner page
    # `title` is used for nav, `page_title` for inner pages
    page_title = models.CharField(max_length=30)

    content_panels = Page.content_panels + [
        FieldPanel('page_title'),
    ]

    subpage_types = ['event_page.EventPage']
