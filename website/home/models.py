from django.db import models

from wagtail.core.models import Page

from wagtail.core.models import Page
from wagtail.core.fields import StreamField, RichTextField
from wagtail.core import blocks
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.core.blocks import PageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel
from website.core.blocks import InfoSection
from website.core.forms import EmailForm
from django.http import HttpResponseRedirect

from events.models import Email


class HomePage(Page):

    def get_context(self, request):
        context = super().get_context(request)
        context['form'] = EmailForm()
        return context

    def serve(self, request):
        if request.method == "POST":
            # create a form instance and populate it with data from the request:
            form = EmailForm(request.POST)
            # check whether it's valid:
            if form.is_valid():
                # process the data in form.cleaned_data as required
                email = form.cleaned_data['email']
                Email.objects.create(
                    email=email,
                )
        return super().serve(request)

    title_left = models.CharField(max_length=15)
    title_right = models.CharField(max_length=15)
    title_logo = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    body = StreamField(
        [
            ('infosection', InfoSection())
        ]
    )

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('title_left'),
                FieldPanel('title_right'),
                ImageChooserPanel('title_logo'),
            ]
        ),
        StreamFieldPanel('body'),
    ]
