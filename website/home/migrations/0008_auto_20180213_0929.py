# Generated by Django 2.0.1 on 2018-02-13 14:29

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0007_auto_20180212_1605'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepage',
            name='body',
            field=wagtail.core.fields.StreamField((('infosection', wagtail.core.blocks.StructBlock((('headline', wagtail.core.blocks.CharBlock(required=False)), ('body', wagtail.core.blocks.RichTextBlock(required=False)), ('call_to_action_target', wagtail.core.blocks.PageChooserBlock(required=False)), ('call_to_action_text', wagtail.core.blocks.CharBlock(required=False))))),)),
        ),
    ]
