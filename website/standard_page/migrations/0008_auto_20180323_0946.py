# Generated by Django 2.0.1 on 2018-03-23 13:46

from django.db import migrations
import wagtail.core.fields
import wagtailmodelchooser.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('standard_page', '0007_auto_20180322_1354'),
    ]

    operations = [
        migrations.AlterField(
            model_name='standardpage',
            name='sidebar',
            field=wagtail.core.fields.StreamField((('event_filter', wagtailmodelchooser.blocks.ModelChooserBlock(target_model='events.tag', template='core/blocks/sidebar_event.html')),)),
        ),
    ]
