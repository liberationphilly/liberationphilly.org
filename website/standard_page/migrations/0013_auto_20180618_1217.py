# Generated by Django 2.0.1 on 2018-06-18 16:17

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks
import wagtailmodelchooser.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('standard_page', '0012_auto_20180616_1445'),
    ]

    operations = [
        migrations.AlterField(
            model_name='standardpage',
            name='sidebar',
            field=wagtail.core.fields.StreamField((('event_filter', wagtailmodelchooser.blocks.ModelChooserBlock(target_model='events.tag', template='core/blocks/sidebar_event.html')), ('Donate', wagtail.core.blocks.StructBlock(())), ('CTA', wagtail.core.blocks.StructBlock((('headline', wagtail.core.blocks.CharBlock(required=False)), ('body', wagtail.core.blocks.RichTextBlock(required=False)), ('call_to_action_target', wagtail.core.blocks.URLBlock(required=False)), ('call_to_action_text', wagtail.core.blocks.CharBlock(required=False)), ('image', wagtail.images.blocks.ImageChooserBlock()))))), blank=True),
        ),
    ]
