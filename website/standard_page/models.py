from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.images.blocks import ImageChooserBlock
from website.core.blocks import InfoSection, PolaroidBlock, DonateBlock, SidebarCTABlock, SubscribeBlock

from wagtailmodelchooser.blocks import ModelChooserBlock


class StandardPage(Page):
    """
    Generic page class used by all inner site pages.
    """

    # Overrides the title displayed on the inner page
    # `title` is used for nav, `page_title` for inner pages
    page_title = models.CharField(max_length=30)

    body = StreamField(
        [
            ('infosection', InfoSection()),
            ('featuredimage', ImageChooserBlock()),
            ('Polaroids', PolaroidBlock()),
            ('Donate', DonateBlock()),
            ('Subscribe', SubscribeBlock()),
        ]
    )

    sidebar = StreamField(
        [
            ('event_filter', ModelChooserBlock(
                'events.Tag',
                template="core/blocks/sidebar_event.html",
            )),
            ('Donate', DonateBlock()),
            ('CTA', SidebarCTABlock()),

        ],
        blank=True
    )

    content_panels = Page.content_panels + [
        FieldPanel('page_title'),
        StreamFieldPanel('body'),
        StreamFieldPanel('sidebar'),
    ]
